FROM --platform=amd64 alpine:3.17.1
LABEL maintainer="rayne.adm@gmail.com"
COPY public/info.txt /opt
ENTRYPOINT ["cat", "/opt/info.txt"]